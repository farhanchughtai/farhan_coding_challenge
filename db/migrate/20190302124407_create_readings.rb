class CreateReadings < ActiveRecord::Migration[5.2]
  def change
    create_table :readings do |t|
      t.integer :thermostat_id, null: false
      t.integer :number, null: false
      t.float :temperature, null: true
      t.float :humidity, null: true
      t.float :battery_charge, null: true
      t.references :thermostat, foreign_key: true,  null: false, default: ""
      t.timestamps
    end
    add_index :readings, [:number, :thermostat_id], :unique => true
  end
end
