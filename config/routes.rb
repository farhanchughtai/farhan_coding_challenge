require 'sidekiq/web'
Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do
    namespace :v1, format: :json do
      post '/readings/create', to: 'readings#create'
      get 'readings/:id/thermostat', to: 'readings#thermostat'
      get 'readings/stats', to: 'readings#stats'
    end
  end
  match "/404", :to => "errors#not_found", :via => :all
  match "/500", :to => "errors#internal_server_error", :via => :all
  mount Sidekiq::Web, at: 'sidekiq', as: :sidekiq
end
