class Reading < ApplicationRecord

  attr_accessor :skip_reading_attributes

  validates_presence_of :thermostat_id, :number
  validates_presence_of :temperature, :humidity, :battery_charge, unless: -> {self.skip_reading_attributes == true}
  validates :number, :uniqueness => {scope: :thermostat_id}

  belongs_to :thermostat

  before_validation :generate_number

  private
    def generate_number
      self.number = self.thermostat.max_reading_number unless self.number.present?
    end

end
