class Thermostat < ApplicationRecord

  validates_presence_of :household_token, :location
  validates_uniqueness_of :household_token

  has_many :readings

  def max_reading_number
    self.readings.present? ? (self.readings.maximum("number") + 1): (1)
  end
end
