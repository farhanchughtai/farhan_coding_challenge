class ReadingWorker
  include Sidekiq::Worker

  def perform(reading_id, data)
    reading = Reading.find(reading_id)
    data = eval(data);
    reading.temperature = data["temperature"].to_f
    reading.humidity = data["humidity"].to_f
    reading.battery_charge = data["battery_charge"].to_f
    reading.save
  end
end
