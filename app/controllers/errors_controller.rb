class ErrorsController < ActionController::Base
  protect_from_forgery with: :null_session

  def not_found
    render json: {errors: [{detail: "Not Found"}] }, status: 404
  end

  def internal_server_error
    render json: {errors: [{detail: "Something went wrong"}] }, status: 404
  end

end