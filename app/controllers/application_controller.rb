class ApplicationController < ActionController::API
  before_action :require_household_token!
  
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  rescue_from NameError, with: :error_occurred
  rescue_from ActionController::RoutingError, with: :error_occurred
  rescue_from ActionController::UnknownFormat, with: :error_occurred
  rescue_from ActiveRecord::RecordInvalid, with: :render_unprocessable_entity_response

  private
    def require_household_token!
      return true if params[:household_token].present? && authenticate_household_token(params[:household_token]) 
      render json: { errors: [ { detail: "Household Token is not found!" } ] }, status: 401
    end

    def authenticate_household_token(household_token)
      @thermostat = Thermostat.find_by(household_token: household_token)
    end

  protected
    def record_not_found(error)
      render json: {errors: [{detail: error.to_json}] }, status: 404
    end
    
    def error_occurred(error)
      render json: {errors: [{detail: error.to_json}] }, status: 500
    end

    def render_unprocessable_entity_response(error)
      render json: {errors: [{detail: error.to_json}] }, status: 422
    end
end
