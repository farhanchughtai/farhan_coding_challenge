class Api::V1::ReadingsController < ApplicationController

  before_action :check_reading_params, only: :create

  def create
    reading = Reading.new
    reading.thermostat_id = @thermostat.id
    reading.skip_reading_attributes = true
    if reading.save
      ReadingWorker.perform_async(reading.id,reading_params)
      render json: { id: reading.id, sequence_number: reading.number }, status: 200
    else
      render json: { errors: [ { detail: reading.errors.to_json } ] }, status: 500
    end
  end

  def thermostat
    readings = @thermostat.readings
    if readings.where(id: params[:id]).present?
      render json: { thermostat: @thermostat, readings: readings}, status: 200
    else
      render json: {error: "Reading is not found against household token"}, status: 404
    end
    
  end

  def stats
    readings = @thermostat.readings

    avg_temperature = (readings.average("temperature::float").to_f).round(2)
    min_temperature = (readings.minimum("temperature::float").to_f).round(2)
    max_temperature = (readings.maximum("temperature::float").to_f).round(2)

    avg_humidity = (readings.average("humidity::float").to_f).round(2)
    min_humidity = (readings.minimum("humidity::float").to_f).round(2)
    max_humidity = (readings.maximum("humidity::float").to_f).round(2)

    avg_battery_charge = (readings.average("battery_charge::float").to_f).round(2)
    min_battery_charge = (readings.minimum("battery_charge::float").to_f).round(2)
    max_battery_charge = (readings.maximum("battery_charge::float").to_f).round(2)

    render json: {
      average_temperature: avg_temperature, maximum_temperature: max_temperature, minimum_temperature: min_temperature,
      average_humidity: avg_humidity, maximum_humidity: max_humidity, minimum_humidity: min_humidity,
      average_battery_charge: avg_battery_charge, maximum_battery_charge: max_battery_charge, minimum_battery_charge: min_battery_charge
      }, status: 200
  end

  private

    def reading_params
      params.require(:reading).permit(:id, :thermostat_id, :number, :temperature, :humidity, :battery_charge)
    end

    def check_reading_params
      valid_params = reading_params
      (valid_params[:temperature].present? && is_number?(valid_params[:temperature]) && valid_params[:humidity].present?  && is_number?(valid_params[:humidity]) && valid_params[:battery_charge].present?  && is_number?(valid_params[:battery_charge])) ? true : head(400)
    end

    def is_number? string
      true if Float(string) rescue false
    end
end