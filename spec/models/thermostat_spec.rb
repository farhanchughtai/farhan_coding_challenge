require 'rails_helper'

RSpec.describe Thermostat, type: :model do
  before(:all) do
    @thermostat = FactoryGirl.build(:thermostat, household_token: Faker::Number.number(10), location: Faker::Address.street_address)
  end

  describe "Validations" do
    it "is valid with valid attributes" do
      expect(@thermostat).to be_valid
    end
    it "is not valid without a household_token" do
      @thermostat.household_token = nil
      expect(@thermostat).to_not be_valid
    end
    it "is not valid without a location" do
      @thermostat.location = nil
      expect(@thermostat).to_not be_valid
    end
  end
  
  describe "Associations" do
    it "has many readings" do
      assc = described_class.reflect_on_association(:readings)
      expect(assc.macro).to eq :has_many
    end  
  end
end