require 'rails_helper'

RSpec.describe Reading, type: :model do
  before(:all) do
    @thermostat = FactoryGirl.create(:thermostat, household_token: Faker::Number.number(10), location: Faker::Address.street_address)
    @reading = FactoryGirl.create(:reading, temperature: 22.2, humidity: 5.2, battery_charge: 2.3, thermostat_id: @thermostat.id)
  end

  describe "Validations" do
    it "is valid with valid attributes" do
      expect(@reading).to be_valid
    end

    it "is not valid without a temperature" do
      @reading.temperature = nil
      expect(@reading).to_not be_valid
    end

    it "is not valid without a humidity" do
      @reading.humidity = nil
      expect(@reading).to_not be_valid
    end

    it "is not valid without a battery_charge" do
      @reading.battery_charge = nil
      expect(@reading).to_not be_valid
    end

    it "It has sequence number for every household" do
      thermostat_2 = FactoryGirl.create(:thermostat, household_token: Faker::Number.number(10), location: Faker::Address.street_address)
      reading_2 = FactoryGirl.create(:reading, temperature: 22.2, humidity: 5.2, battery_charge: 2.3, thermostat_id: @thermostat.id)
      reading_3 = FactoryGirl.create(:reading, temperature: 2.2, humidity: 2.2, battery_charge: 4.3, thermostat_id: @thermostat.id)
      reading_4 = FactoryGirl.create(:reading, temperature: 2.2, humidity: 2.2, battery_charge: 4.3, thermostat_id: thermostat_2.id)
      reading_5 = FactoryGirl.create(:reading, temperature: 2.2, humidity: 2.2, battery_charge: 4.3, thermostat_id: thermostat_2.id)
      
      expect(@reading.number).to eql(1)
      expect(reading_2.number).to eql(2)
      expect(reading_3.number).to eql(3)
      expect(reading_4.number).to eql(1)
      expect(reading_5.number).to eql(2)
    end
  end
  
  describe "Associations" do
    it "belongs to thermostat" do
      assc = described_class.reflect_on_association(:thermostat)
      expect(assc.macro).to eq :belongs_to
    end  
  end
end