require 'rails_helper'

RSpec.describe Api::V1::ReadingsController do
  before(:all) do
    @thermostat = FactoryGirl.create(:thermostat, household_token: Faker::Number.number(10), location: Faker::Address.street_address)
  end

  describe "POST /api/v1/readings/create" do
    context "with valid parameters" do
      it "creates a new reading" do
        valid_params = {
          reading: {
            temperature: "22.2",
            humidity: "3.3",
            battery_charge: "5.5"
          },
          household_token: @thermostat.household_token
        }
        expect { post "create", params: valid_params }.to change(Reading, :count).by(+1)
        expect(response).to have_http_status 200
        expect(response.body).to include("id")
        expect(response.body).to include("number")
      end    
    end

    context "with invalid parameters" do      
      it "will not create a reading with missing params" do
        invalid_params = {
          reading: {
            temperature: "asd",
            humidity: "abc",
            battery_charge: "def"
          },
          household_token: @thermostat.household_token
        }
        post "create", params: invalid_params
        expect(response).to have_http_status 400
      end

      it "will not create a reading with invalid type params" do
        invalid_params = {
          reading: {
            temperature: "22.2"          
          },
          household_token: @thermostat.household_token
        }
        post "create", params: invalid_params
        expect(response).to have_http_status 400
      end

      it "will not create a reading without household_token" do
        invalid_params = {
          reading: {
            temperature: "22.2",
            humidity: "3.3",
            battery_charge: "5.5"          
          }
        }
        post "create", params: invalid_params
        expect(response).to have_http_status 401
      end

      it "will not create a reading with wrong household_token" do
        invalid_params = {
          reading: {
            temperature: "22.2",
            humidity: "3.3",
            battery_charge: "5.5"          
          },
          household_token: 'asdasd'
        }
        post "create", params: invalid_params
        expect(response).to have_http_status 401
      end
    end
  end

  describe "POST /api/v1/thermostat" do
    context "with valid data" do
      it "returns thermostat info from reading id" do
        reading = FactoryGirl.create(:reading, temperature: 22.2, humidity: 5.2, battery_charge: 2.3, thermostat_id: @thermostat.id)
        get "thermostat", params: {id: reading.id, household_token: @thermostat.household_token}
        data = JSON.parse(response.body)
        readings_data = data["readings"]
        expect(response).to have_http_status 200
        expect(data["thermostat"]["household_token"]).to eql(@thermostat.household_token)
        expect(data["thermostat"]["location"]).to eql(@thermostat.location)
        expect(readings_data).to be_present
        expect(readings_data.count).to be > 0
        expect(readings_data.count).to eql @thermostat.readings.count
      end
    end

    context "with invalid data" do
      it "will not return thermostat data without household_token" do
        reading = FactoryGirl.create(:reading, temperature: 22.2, humidity: 5.2, battery_charge: 2.3, thermostat_id: @thermostat.id)
        get "thermostat", params: {id: reading.id }
        expect(response).to have_http_status 401
      end

      it "will not return thermostat data with wrong household_token" do
        reading = FactoryGirl.create(:reading, temperature: 22.2, humidity: 5.2, battery_charge: 2.3, thermostat_id: @thermostat.id)
        get "thermostat", params: {id: reading.id, household_token: 'asdadsd' }
        expect(response).to have_http_status 401
      end

      it "will not return thermostat data with wrong reading id" do
        get "thermostat", params: {id: 1000, household_token: @thermostat.household_token }
        expect(response).to have_http_status 404
      end

      it "will not return thermostat data with wrong reading id of different thermostat household_token" do
        thermostat_1 = FactoryGirl.create(:thermostat, household_token: Faker::Number.number(10), location: Faker::Address.street_address)
        reading = FactoryGirl.create(:reading, temperature: 22.2, humidity: 5.2, battery_charge: 2.3, thermostat_id: @thermostat.id)
        reading_1 = FactoryGirl.create(:reading, temperature: 22.2, humidity: 5.2, battery_charge: 2.3, thermostat_id: thermostat_1.id)
        get "thermostat", params: {id: reading.id, household_token: thermostat_1.household_token }
        expect(response).to have_http_status 404
      end
    end
  end

  describe "POST /api/v1/stats" do
    it "returns thermostat maximum, minumum and average readings" do
      thermostat = FactoryGirl.create(:thermostat, household_token: Faker::Number.number(10), location: Faker::Address.street_address)
      reading_1 = FactoryGirl.create(:reading, temperature: 2.0, humidity: 1.2, battery_charge: 0.3, thermostat_id: thermostat.id)
      reading_2 = FactoryGirl.create(:reading, temperature: 4.0, humidity: 3.2, battery_charge: 1.3, thermostat_id: thermostat.id)
      reading_3 = FactoryGirl.create(:reading, temperature: 6.0, humidity: 4.2, battery_charge: 2.3, thermostat_id: thermostat.id)  
      
      get "stats", params: {household_token: thermostat.household_token }
      expect(response).to have_http_status 200
      data = eval(response.body)
      expect(data[:minimum_temperature]).to eql 2.0
      expect(data[:average_temperature]).to eql 4.0
      expect(data[:maximum_temperature]).to eql 6.0

      expect(data[:minimum_humidity]).to eql 1.2
      expect(data[:average_humidity]).to eql 2.87
      expect(data[:maximum_humidity]).to eql 4.2

      expect(data[:minimum_battery_charge]).to eql 0.3
      expect(data[:average_battery_charge]).to eql 1.3
      expect(data[:maximum_battery_charge]).to eql 2.3
    end

    it "will not returns thermostat maximum, minumum and average readings without household token" do
      thermostat = FactoryGirl.create(:thermostat, household_token: Faker::Number.number(10), location: Faker::Address.street_address)
      get "stats"
      expect(response).to have_http_status 401
    end

    it "will not returns thermostat maximum, minumum and average readings with wrong household token" do
      thermostat = FactoryGirl.create(:thermostat, household_token: Faker::Number.number(10), location: Faker::Address.street_address)
      reading_1 = FactoryGirl.create(:reading, temperature: 2.0, humidity: 5.2, battery_charge: 2.3, thermostat_id: thermostat.id)
      reading_2 = FactoryGirl.create(:reading, temperature: 4.0, humidity: 5.2, battery_charge: 2.3, thermostat_id: thermostat.id)
      reading_3 = FactoryGirl.create(:reading, temperature: 6.0, humidity: 5.2, battery_charge: 2.3, thermostat_id: thermostat.id)  
      
      get "stats", params: {household_token: 'akdfjf' }
      expect(response).to have_http_status 401
    end
  end

  
end