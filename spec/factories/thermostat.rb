FactoryGirl.define do
  factory :thermostat, class: Thermostat do
    household_token Faker::Number.number(10)
    location Faker::Address.street_address
  end
end