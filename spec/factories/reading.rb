FactoryGirl.define do
  factory :reading, class: Reading do
    temperature 23.4
    humidity 5.2
    battery_charge 2.3
    thermostat
  end
end